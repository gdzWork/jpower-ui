import store from '@/store/';

function callkack(el, binding, vnode){
    if (store.getters.menuBtnPer && store.getters.menuBtnPer.length > 0){
        if (typeof(binding.value) === 'object'){
            for (let key in binding.value) {
                if (vnode.componentOptions.propsData.option){
                    //如果没有则代表没有权限需要不显示按钮
                    if (!store.getters.menuBtnPer.includes(binding.value[key])){
                        vnode.componentOptions.propsData.option[key] = false;
                    }
                }
            }
        } else {
            //前面!开头代表没有权限的时候显示
            if (binding.value.indexOf('!') === 0){
                if (store.getters.menuBtnPer.includes(binding.value.slice(1))){
                    el.remove();
                }
            } else {
                //如果没有则代表没有权限需要删除掉按钮
                if (!store.getters.menuBtnPer.includes(binding.value)){
                    el.remove();
                }
            }
        }
    }
}
let button = {
    directives: {
        permission: {
            // 指令的定义
            inserted: function (el, binding, vnode) {
                callkack(el, binding, vnode)
            },
            update: function (el, binding, vnode) {
                callkack(el, binding, vnode)
            },
        }
    },
    data(){
        return{
        };
    },
    created() {
    },
    mounted() {
    },
    methods:{
    }
};

export default button;