import Vue from 'vue';
Vue.config.errorHandler = function (err, vm, info) {

  Vue.nextTick(() => {

    console.group('>>>>>> 错误信息 >>>>>>')
    console.error(info)
    console.groupEnd();
    console.group('>>>>>> Vue 实例 >>>>>>')
    console.log(vm)
    console.groupEnd();
    console.group('>>>>>> Error >>>>>>')
    console.log(err)
    console.groupEnd();

  })
}