export default (safe)=>{
  //safe => vue的this对象
  return {
    menuWidth: 300,
    labelWidth: 120,
    selection: true,
    indexLabel: '序号',
    tip: false,
    index: true,
    border: true,
    stripe: true,
    viewBtn: true,
    dialogClickModal: false,
    dialogDrag: true,
    searchMenuSpan: 6,
    searchIndex: 3,
    searchIcon: true,
    column: [{
        label: "名称",
        prop: "name",
        search: true,
        rules: [{
          required: true,
          message: "请输入岗位名称",
          trigger: "blur"
        }]
      },
      {
        label: "编码",
        prop: "code",
        search: true,
        rules: [{
          required: true,
          message: "请输入岗位编码",
          trigger: "blur"
        }]
      },
      {
        label: "类型",
        prop: "type",
        search: true,
        slot: true,
        type: 'select',
        dataType: 'number',
        dicUrl: window.urllist.dictUrl + 'POST_TYPE',
        props: {
          label: "name",
          value: "code"
        },
        rules: [{
          required: true,
          message: "请输入岗位类型",
          trigger: "blur"
        }]
      },
      {
        label: "排序",
        prop: "sort",
        type: 'number',
        showColumn: false,
        hide: true,
        value: 0,
        precision: 0
      },
      {
        label: "岗位描述",
        prop: "describe",
        type: 'textarea',
        showColumn: false,
        hide: true
      },
      {
        label: "上岗条件",
        prop: "condition",
        type: 'textarea',
        showColumn: false,
        hide: true
      },
      {
        label: '是否启用',
        prop: 'status',
        type: "switch",
        width: 80,
        dataType: 'number',
        align: "center",
        search: true,
        slot: true,
        span: 12,
        value: 1,
        dicUrl: window.urllist.dictUrl + 'YN01',
        props: {
          label: "name",
          value: "code"
        },
        rules: [
          {
            required: true,
            message: "请选择是否启用",
            trigger: "blur"
          }
        ]
      },{
        label: "岗位人数",
        prop: "userNum",
        slot: true,
        type: 'number',
        showColumn: true,
        display: false
      },{
        label: "所属租户",
        prop: "tenantCode",
        type: 'tree',
        filterable: true,
        search: safe.$store.getters.isShowTenantCode,
        dicUrl: window.urllist.tenantSelectors,
        display: safe.$store.getters.isShowTenantCode,
        disabled: true,
        value: safe.$store.getters.tenantCode,
        hide: !safe.$store.getters.isShowTenantCode,
        props: {
          label: 'tenant_name',
          value: 'tenant_code'
        }
      }],
  }
}