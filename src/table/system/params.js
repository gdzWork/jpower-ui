export default ()=> {
  //safe => vue的this对象
  return {
    menuWidth: 300,
    labelWidth: 120,
    selection: true,
    indexLabel: '序号',
    tip: false,
    index: true,
    align: 'center',
    headerAlign: 'center',
    border: true,
    stripe: true,
    viewBtn: true,
    dialogClickModal: false,
    dialogDrag: true,
    searchMenuSpan: 6,
    searchIndex: 3,
    searchIcon: true,
    column: [{
        label: "参数名称",
        prop: "name",
        search: true,
        rules: [{
          required: true,
          message: "请输入参数名称",
          trigger: "blur"
        }]
      },
      {
        label: "参数编码",
        prop: "code",
        search: true,
        rules: [{
          required: true,
          message: "请输入参数编码",
          trigger: "blur"
        }]
      },
      {
        label: "参数值",
        prop: "value",
        search: true,
        span: 24,
        rules: [{
          required: true,
          message: "请输入参数值",
          trigger: "blur"
        }]
      },
      {
        label: "备注",
        prop: "note",
        showColumn: false,
        hide: true,
        type: "textarea",
        span: 24,
        minRows: 6
      }],
  }
}