
import iconList from "@/config/iconList";
import {treeAddDisabled, treeFindById} from "@/util/util";
export default (safe)=> {
  //safe => vue的this对象
  return {
    index: true,
    indexLabel: '序号',
    lazy: true,
    tip: false,
    border: true,
    viewBtn: true,
    selection: true,
    menuWidth: 280,
    dialogClickModal: false,
    dialogDrag: true,
    searchMenuSpan: 6,
    searchIndex: 3,
    searchIcon: true,
    column: [{
      label: "名称",
      prop: "functionName",
      search: true,
      span: 12,
      rules: [{
        required: true,
        message: "请输入名称",
        trigger: "blur"
      }]
    },
      {
        label: "别名",
        prop: "alias",
        search: true,
        span: 12,
        hide: true,
        rules: [
          {
            required: true,
            message: "请输入别名",
            trigger: "blur"
          }
        ]

      },{
        label: '编号',
        prop: 'code',
        search: true,
        overHidden: true,
        span: 12,
        rules: [
          {
            required: true,
            message: "请输入编号",
            trigger: "blur"
          }
        ]
      },{
        label: '打开方式',
        prop: 'target',
        span: 12,
        hide: true,
        slot: true,
        align: "center",
        type: 'select',
        value: '_self',
        dicUrl: window.urllist.dictUrl + 'DKFS',
        props: {
          label: "name",
          value: "code"
        },
        rules: [
          {
            required: true,
            message: "请选择页面打开方式",
            trigger: "blur"
          }
        ]
      }, {
        label: "接口地址",
        prop: "url",
        search: true,
        span: 24,
        overHidden: true,
        rules: [
          {
            required: true,
            message: "请输入接口地址",
            trigger: "blur"
          }
        ]
      },
      {
        label: '功能类型',
        prop: 'functionType',
        type: "radio",
        overHidden: true,
        width: 80,
        dataType: 'number',
        border: true,
        slot: true,
        span: 12,
        value: 1,
        dicUrl: window.urllist.dictUrl + 'FUNCTION_TYPE',
        props: {
          label: "name",
          value: "code"
        },
        rules: [
          {
            required: true,
            message: "请选择功能类型",
            trigger: "blur"
          }
        ]
      },
      {
        label: "上级菜单",
        prop: "parentId",
        type: "tree",
        dataType: 'string',
        dicUrl: window.urllist.system + '/core/function/treeMenuTypeByClientId?clientId='+safe.clientId,
        showColumn: false,
        dicFlag: true,
        hide: true,
        span: 12,
        popperClass: 'custom-select-tree-node',
        change: ({value,item}) => {

          if(value !== undefined && value !== '-1' && item === undefined){
            item = treeFindById(safe.$refs.crud.data, value, "id");
          }

          const json = safe.$refs.crud.DIC.functionType;

          if (item === undefined){
            json.forEach(f=>{
              f.disabled = false;
            });
            return;
          }

          json.forEach(f=>{
            if (item.functionType === 0 && f.code === 1){
              f.disabled = true;

              if (safe.form.functionType === 1){
                safe.form.functionType = 2;
              }

            } else {
              f.disabled = false;
            }
          });

        },
        dicFormatter: (data)=>{
          return treeAddDisabled(data.data || [], safe.form.id);
        },
        props: {
          label: "functionName",
          value: 'id'
        },
        rules: [
          {
            required: false,
            message: "请选择上级菜单",
            trigger: "click"
          }
        ]
      },
      {
        label: "隐藏",
        prop: "isHide",
        slot: true,
        display: false,
        value: false,
        width: 80,
        align: "center",
        span: 12
      },
      {
        label: "图标",
        prop: "icon",
        type: "icon",
        slot: true,
        width: 80,
        align: "center",
        span: 12,
        iconList: iconList
      },
      {
        label: "排序",
        prop: "sort",
        value: 1,
        span: 12,
        width: 80,
        type: "number",
        hide: true,
        showColumn: false,
        rules: [
          {
            required: true,
            message: "请输入排序",
            trigger: "blur"
          }
        ]
      },
      {
        label: "模块概述",
        prop: "moudeSummary",
        type: "textarea",
        span: 24,
        minRows: 6,
        hide: true
      },
      {
        label: "操作说明",
        prop: "operateInstruction",
        type: "textarea",
        span: 24,
        minRows: 6,
        hide: true
      }, {
        label: "备注",
        prop: "remark",
        type: "textarea",
        span: 24,
        minRows: 6,
        hide: true
      }
    ]
  }
}