import iconList from "@/config/iconList";
export default ()=>{
  //safe => vue的this对象
  return {
    menuWidth: 300,
    labelWidth: 120,
    selection: true,
    indexLabel: '序号',
    tip: false,
    index: true,
    align: 'center',
    headerAlign: 'center',
    border: true,
    stripe: true,
    viewBtn: true,
    dialogClickModal: false,
    dialogDrag: true,
    searchMenuSpan: 6,
    searchIndex: 3,
    searchIcon: true,
    column: [{
        label: "名称",
        prop: "name",
        search: true,
        rules: [{
          required: true,
          message: "请输入菜单名称",
          trigger: "blur"
        }]
      },
      {
        label: "编码",
        prop: "code",
        search: true,
        rules: [{
          required: true,
          message: "请输入菜单编码",
          trigger: "blur"
        }]
      },
      {
        label: "图标",
        prop: "icon",
        type: "icon",
        slot: true,
        width: 80,
        align: "center",
        span: 12,
        iconList: iconList
      },
      {
        label: "首页",
        prop: "router"
      },
      {
        label: "是否启用",
        prop: "status",
        search: true,
        value: 1,
        slot: true,
        editDisplay: false,
        type: 'switch',
        dataType: 'number',
        dicUrl: window.urllist.dictUrl + 'YN01',
        props: {
          label: "name",
          value: "code"
        },
        rules: [{
          required: true,
          message: "请选择菜单状态",
          trigger: "blur"
        }]
      },
      {
        label: "排序",
        prop: "sortNum",
        type: 'number',
        showColumn: false,
        hide: true,
        value: 0,
        precision: 0
      },
      {
        label: "备注",
        prop: "note",
        span: 24,
        type: 'textarea',
        showColumn: false,
        hide: true
      }],
  }
}