
export default (safe)=> {
  //safe => vue的this对象
  return {
    index: true,
    indexLabel: '序号',
    selection: true,
    tip: false,
    border: true,
    stripe: true,
    viewBtn: true,
    searchMenuSpan: 6,
    searchIndex: 3,
    searchIcon: true,
    dialogClickModal: false,
    dialogDrag: true,
    column: [{
      label: "用户名",
      prop: "loginId",
      overHidden: true,
      search: true,
      width: 150,
      display: false,
      rules: [{
        required: true,
        message: "请输入用户名",
        trigger: "blur"
      }]
    },{
      label: "在线状态",
      prop: "onLine",
      width: 80,
      slot: true,
      display: false
    },
      {
        label: '用户昵称',
        prop: 'nickName',
        overHidden: true,
        display: false,
        search: true,
        width: 150,
        rules: [{
          required: true,
          message: "请输入昵称",
          trigger: "blur"
        }]
      },
      {
        label: "所属租户",
        prop: "tenantCode",
        type: 'tree',
        search: safe.$store.getters.isShowTenantCode,
        dicUrl: window.urllist.tenantSelectors,
        display: false,
        hide: !safe.$store.getters.isShowTenantCode,
        props: {
          label: 'tenant_name',
          value: 'tenant_code'
        }
      },
      {
        label: "用户类型",
        prop: "userType",
        slot: true,
        type: "select",
        search: true,
        dataType: 'number',
        dicUrl: window.urllist.dictUrl + 'USER_TYPE',
        width: 100,
        props: {
          label: 'name',
          value: 'code'
        },
        display: false
      },
      {
        label: "部门",
        prop: "orgName",
        display: false,
        overHidden: true
      },
      {
        label: "岗位",
        prop: "postId",
        type: 'select',
        search: true,
        filterable: true,
        display: false,
        dataType: 'string',
        dicUrl: window.urllist.user + '/core/post/select',
        props: {
          label: 'name',
          value: 'id'
        }
      },
      {
        label: "角色",
        prop: "roleName",
        display: false,
        overHidden: true,
        width: 150
      },
       {
        label: '最近登陆时间',
        prop: 'lastLoginTime',
        display: false,
        width: 150,
        detail: true
      }, {
        label: '登陆次数',
        prop: 'loginCount',
        display: false,
        detail: true
      }],
    group: [{
      label: '基础信息',
      prop: 'jcxx',
      icon: "el-icon-user-solid",
      column: [{
        label: "登录名",
        prop: "loginId",
        rules: [{
          required: true,
          message: "请输入用户名",
          trigger: "blur"
        },
          {
            pattern: /^[a-zA-Z0-9]{1,}$/,
            message: '只能输入数字或字母'
          }]
      }, {
        label: "所属租户",
        prop: "tenantCode",
        type: 'tree',
        filterable: true,
        search: safe.$store.getters.isShowTenantCode,
        dicUrl: window.urllist.tenantSelectors,
        display: safe.$store.getters.isShowTenantCode,
        disabled: false,
        showColumn: false,
        hide: true,
        props: {
          label: 'tenant_name',
          value: 'tenant_code'
        },
        rules: [{
          required: true,
          message: "请选择所属租户",
          trigger: "blur"
        }]
      },
        {
          label: "用户类型",
          prop: "userType",
          type: "select",
          span: 24,
          dataType: 'number',
          dicUrl: window.urllist.dictUrl + 'USER_TYPE',
          props: {
            label: 'name',
            value: 'code'
          },
          rules: [{
            required: true,
            message: "请选择用户类型",
            trigger: "blur"
          }],
        },
        {
          label: "部门",
          prop: "orgId",
          type: 'tree',
          dataType: 'string',
          dicData: [],
          multiple: false,
          checkStrictly: true,
          props: {
            label: 'name',
            value: 'id'
          },
          rules: [{
            required: true,
            message: "请选择部门",
            trigger: "blur"
          }],
        },
        {
          label: "岗位",
          prop: "postId",
          type: 'select',
          search: true,
          filterable: true,
          dataType: 'string',
          dicData: [],
          props: {
            label: 'name',
            value: 'id'
          }
        },
        {
          label: '用户昵称',
          prop: 'nickName',
          rules: [{
            required: true,
            message: "请输入昵称",
            trigger: "blur"
          }]
        }, {
          label: '是否激活',
          prop: 'activationStatus',
          slot: true,
          dicUrl: window.urllist.dictUrl + 'YN01',
          dataType: 'number',
          type: "select",
          props: {
            label: "name",
            value: "code"
          },
          rules: [{
            required: true,
            message: "请选择是否激活",
            trigger: "blur"
          }]
        }]
    }, {
      label: '基本信息',
      prop: 'jbxx',
      icon: 'el-icon-phone',
      column: [{
        label: "用户姓名",
        prop: "userName"
      }, {
        label: '出生日期',
        prop: 'birthday',
        type: 'datetime',
        format: 'yyyy-MM-dd',
        valueFormat: 'yyyy-MM-dd',
        hide: true
      }, {
        label: '证件类型',
        prop: 'idType',
        type: 'select',
        dicUrl: window.urllist.dictUrl + 'ID_TYPE',
        dataType: 'number',
        props: {
          label: 'name',
          value: 'code'
        },
      }, {
        label: '证件号码',
        prop: 'idNo',
        hide: true
      }, {
        label: '电子邮箱',
        prop: 'email',
        hide: true
      }, {
        label: '电话',
        prop: 'telephone',
        maxlength: 11,
        showWordLimit: true,
        hide: true,
        rules: [{
          required: true,
          message: "请输入 电话",
          trigger: "blur"
        },
          {
            pattern: /(?:0|86|\+86)?1[3-9]\d{9}/,
            message: "电话不合法"
          }]
      }, {
        label: '地址',
        prop: 'address',
        hide: true
      }, {
        label: '邮编',
        prop: 'postCode',
        hide: true
      }]
    }, {
      label: '其他信息',
      icon: 'el-icon-info',
      disabled: true,
      prop: 'qtxx',
      column: [{
        label: '最近登陆时间',
        prop: 'lastLoginTime',
        addDisplay: false,
        width: 150,
        detail: true
      }, {
        label: '登陆次数',
        prop: 'loginCount',
        addDisplay: false,
        detail: true
      }, {
        label: '创建时间',
        prop: 'createTime',
        addDisplay: false,
        detail: true
      }, {
        label: '更新时间',
        prop: 'updateTime',
        addDisplay: false,
        detail: true
      }]
    }]
  }
}