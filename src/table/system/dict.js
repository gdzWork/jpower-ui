
export default (safe)=> {
  //safe => vue的this对象
  return {
    index: true,
    indexLabel: '序号',
    selection: true,
    tip: false,
    border: true,
    lazy: true,
    viewBtn: true,
    rowKey: 'id',
    tree: true,
    labelWidth: 120,
    menuWidth: 300,
    addBtn: !safe.validatenull(safe.oldDictType.id),
    emptyText: safe.oldDictType.dictTypeCode?'暂无数据':'未选择字典类型',
    align: "center",
    dialogClickModal: false,
    dialogDrag: true,
    searchMenuSpan: 6,
    searchIndex: 3,
    searchIcon: true,
    column: [
      {
        label: "字典名称",
        prop: "name",
        align: "left",
        search: true,
        rules: [{
          required: true,
          message: "请输入字典名称",
          trigger: "blur"
        }]
      },
      {
        label: "字典编码",
        prop: "code",
        slot: true,
        search: true,
        rules: [{
          required: true,
          message: "请输入字典编号",
          trigger: "blur"
        }]
      },
      {
        label: "上级字典",
        prop: "parentName",
        disabled: true,
        value: ''-1,
        display: safe.oldDictType.isTree === 1,
        hide: true,
        showColumn: false
      },
      {
        label: "字典类型编码",
        row: true,
        span: 24,
        prop: "dictTypeCode",
        value: safe.oldDictType.dictTypeCode,
        disabled: true,
        rules: [{
          required: true,
          message: "请输入字典名称",
          trigger: "blur"
        }]
      },
      {
        label: "所属语言",
        row: true,
        slot: true,
        prop: "locale",
        border: true,
        value: 'zh',
        type: "radio",
        dataType: 'string',
        align: "center",
        dicUrl: window.urllist.dictUrl + 'YYZL',
        props: {
          label: "name",
          value: "code"
        },
        rules: [{
          required: true,
          message: "请选择所属语言",
          trigger: "blur"
        }]
      },
      {
        label: '是否停用',
        prop: 'isStop',
        type: "switch",
        slot: true,
        width: 80,
        dataType: 'string',
        align: "center",
        span: 12,
        value: 'N',
        dicUrl: window.urllist.dictUrl + 'YN',
        props: {
          label: "name",
          value: "code"
        },
        rules: [
          {
            required: true,
            message: "请选择是否停用",
            trigger: "blur"
          }
        ]
      },
      {
        label: "排序",
        prop: "sortNum",
        type: 'number',
        value: 0,
        showColumn: false,
        hide: true,
        precision: 0
      },
      {
        label: "备注",
        prop: "note",
        type: "textarea",
        span: 24,
        hide: true,
        overHidden: true,
        minRows: 6,
      }],
  }
}