export default (safe)=> {
  //safe => vue的this对象
  return {
    index: true,
    indexLabel: '序号',
    selection: true,
    tip: false,
    tree: true,
    border: true,
    treeProps: {
      hasChildren: 'has'
    },
    viewBtn: true,
    menuWidth: 80,
    menuType: "menu",
    searchMenuSpan: 6,
    searchIndex: 3,
    searchIcon: true,
    dialogClickModal: false,
    dialogDrag: true,
    column: [
      {
        label: "角色名称",
        prop: "name",
        search: true,
        rules: [
          {
            required: true,
            message: "请输入角色名称",
            trigger: "blur"
          }
        ]
      },
      {
        label: "所属租户",
        prop: "tenantCode",
        type: 'tree',
        filterable: true,
        search: safe.$store.getters.isShowTenantCode,
        dicUrl: window.urllist.tenantSelectors,
        display: safe.$store.getters.isShowTenantCode,
        disabled: true,
        hide: !safe.$store.getters.isShowTenantCode,
        props: {
          label: 'tenant_name',
          value: 'tenant_code'
        }
      },
      {
        label: "角色别名",
        prop: "alias",
        search: true,
        rules: [
          {
            required: true,
            message: "请输入角色别名",
            trigger: "blur"
          }
        ]
      },
      {
        label: "上级角色",
        prop: "parentId",
        dicData: [],
        type: "tree",
        hide: true,
        value: safe.$store.getters.rolesId[0],
        props: {
          label: "name",
          value: 'id'
        },
        rules: [
          {
            required: false,
            message: "请选择上级角色",
            trigger: "click"
          }
        ]
      },
      {
        label: "系统角色",
        prop: "isSysRole",
        type: 'switch',
        dicUrl: window.urllist.dictUrl + 'YN01',
        dataType:'number',
        align: 'center',
        value: 0,
        search: true,
        tip: '系统角色无法删除，请谨慎选择',
        tipPlacement: 'top',
        props: {
          label: "name",
          value: "code"
        },
        rules: [
          {
            required: true,
            message: "请选择是否系统角色",
            trigger: "blur"
          }
        ]
      },
      {
        label: "排序",
        prop: "sort",
        type: 'number',
        showColumn: false,
        hide: true,
        precision: 0
      }, {
        label: '备注',
        type: 'textarea',
        span: 24,
        prop: 'remark',
        hide: true,
        showColumn: false
      }

    ]
  }
}