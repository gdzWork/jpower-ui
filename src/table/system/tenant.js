export default ()=> {
  //safe => vue的this对象
  return {
    index: true,
    indexLabel: '序号',
    tip: false,
    border: true,
    viewBtn: true,
    selection: true,
    stripe: true,
    dialogClickModal: false,
    dialogDrag: true,
    searchMenuSpan: 6,
    searchIndex: 3,
    menuWidth: 300,
    searchIcon: true,
    column: [
      {
        label: "租户编码",
        prop: "tenantCode",
        search: true,
        addDisplay: false,
        editDisplay: false,
        width: 100,
        rules: [{
          required: true,
          message: "请输入租户ID",
          trigger: "blur"
        }]
      },
      {
        label: "租户名称",
        prop: "tenantName",
        search: true,
        rules: [{
          required: true,
          message: "请输入参数名称",
          trigger: "blur"
        }]
      },
      {
        label: "联系人",
        prop: "contactName",
        search: true,
        rules: [{
          required: true,
          message: "请输入联系人",
          trigger: "blur"
        }]
      },
      {
        label: "联系电话",
        prop: "contactPhone",
        maxlength: 13,
        showWordLimit: true,
        rules: [{
          pattern: /((([0-9]{3,4}-)?[0-9]{7,8}$)|(1[0-9]{10}$))/,
          message: "联系电话不合法"
        }]
      },
      {
        label: "默认菜单",
        prop: "functionCode",
        viewDisplay: false,
        editDisplay: false,
        addDisplay: true,
        showColumn: false,
        hide: true,
        type: 'cascader',
        multiple: true,
        showAllLevels: false,
        dicUrl: window.urllist.system + '/core/function/clientMenuTree',
        props: {label: 'name',value: 'code'}
      },
      {
        label: "联系地址",
        prop: "address",
        span: 24,
        minRows: 6,
        showColumn: false,
        hide: true,
        type: "textarea"
      },
      {
        label: "账号额度",
        slot: true,
        editDisplay: false,
        viewDisplay: false,
        prop: "accountNumber",
        minRows: 6
      },
      {
        label: "过期时间",
        slot: true,
        editDisplay: false,
        viewDisplay: false,
        prop: "expireTime",
        minRows: 6,
        type: 'date'
      },
      {
        label: "域名地址",
        prop: "domain",
        type: 'url',
        alone: true,
        span: 24
      },
      {
        label: "创建时间",
        prop: "createTime",
        addDisplay: false,
        editDisplay: false,
        span: 24,
        hide: true,
        detail: true
      }
    ]
  }
}