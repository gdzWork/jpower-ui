export default ()=> {
  //safe => vue的this对象
  return {
    index: true,
    indexLabel: '序号',
    translate: false,
    searchLabelWidth: 100,
    labelWidth: 110,
    tip: false,
    align: 'center',
    headerAlign: 'center',
    border: true,
    stripe: true,
    viewBtn: true,
    selection: true,
    dialogClickModal: false,
    dialogDrag: true,
    searchMenuSpan: 6,
    searchIndex: 3,
    searchIcon: true,
    column: [{
      label: "客户端名称",
      prop: "name",
      search: true,
      span: 24,
      rules: [{
        required: true,
        message: "请输入客户端名称",
        trigger: "blur"
      }]
    },

      {
        label: "客户端编码",
        prop: "clientCode",
        search: true,
        rules: [{
          required: true,
          message: "请输入客户端编码",
          trigger: "blur"
        },
          {
            pattern: /^[a-zA-Z0-9]{1,}$/,
            message: '只能输入数字或字母'
          }]
      },

      {
        label: "客户端密钥",
        prop: "clientSecret",
        search: false,
        addDisplay: false,
        rules: [{
          required: true,
          message: "请输入客户端密钥",
          trigger: "blur"
        },
          {
            pattern: /^[a-zA-Z0-9]{1,}$/,
            message: '只能输入数字或字母'
          }]
      },

      {
        label: "登录限制",
        prop: "loginLimit",
        search: false,
        type: "select",
        dicUrl: window.urllist.dictUrl + 'LOGIN_LIMIT',
        props: {
          label: 'name',
          value: 'code'
        },
        rules: [{
          required: true,
          message: "请输入客户端密钥",
          trigger: "blur"
        }]
      },

      {
        label: "令牌时长",
        prop: "accessTokenValidity",
        slot: true,
        append: '秒',
        rules: [{
          required: true,
          message: "请输入token有效时长",
          trigger: "blur"
        }, {
          pattern: /^[1-9]*[0-9][0-9]*$/,
          message: '只能输入正整数'
        }]
      },
      {
        label: "刷新令牌时长",
        prop: "refreshTokenValidity",
        slot: true,
        labelWidth: 120,
        append: '秒',
        rules: [{
          required: true,
          message: "请输入token有效时长",
          trigger: "blur"
        }, {
          pattern: /^[1-9]*[0-9][0-9]*$/,
          message: '只能输入正整数'
        }]
      },
      {
        label: "排序",
        prop: "sortNum",
        type: 'number',
        showColumn: false,
        hide: true,
        precision: 0
      },
      {
        label: "备注",
        prop: "note",
        type: 'textarea',
        showColumn: false,
        hide: true,
        span: 24
      }],
  }
}