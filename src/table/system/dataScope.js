
import iconList from "@/config/iconList";

export default ()=> {
  //safe => vue的this对象
  return {
    lazy: true,
    index: true,
    indexLabel: '序号',
    tip: false,
    border: true,
    viewBtn: false,
    editBtn: false,
    addBtn: false,
    delBtn: false,
    menuWidth: 150,
    dialogClickModal: false,
    dialogDrag: true,
    searchMenuSpan: 4,
    searchIndex: 3,
    searchIcon: true,
    column: [
      {
        label: "菜单名称",
        prop: "functionName",
        search: true,
        rules: [
          {
            required: true,
            message: "请输入菜单名称",
            trigger: "blur"
          }
        ]
      },
      {
        label: "路由地址",
        prop: "url",
        rules: [
          {
            required: true,
            message: "请输入路由地址",
            trigger: "blur"
          }
        ]
      },
      {
        label: "图标",
        prop: "icon",
        type: "icon",
        slot: true,
        width: 80,
        align: "center",
        span: 12,
        iconList: iconList
      },
      {
        label: "菜单编号",
        prop: "code",
        search: true,
        rules: [
          {
            required: true,
            message: "请输入菜单编号",
            trigger: "blur"
          }
        ]
      },
      {
        label: "菜单类型",
        prop: "category",
        type: "radio",
        dicData: [
          {
            label: "菜单",
            value: 1
          },
          {
            label: "按钮",
            value: 2
          }
        ],
        hide: true,
        rules: [
          {
            required: true,
            message: "请选择菜单类型",
            trigger: "blur"
          }
        ]
      },
      {
        label: "菜单别名",
        prop: "alias",
        rules: [
          {
            required: true,
            message: "请输入菜单别名",
            trigger: "blur"
          }
        ]
      }
    ]
  }
}