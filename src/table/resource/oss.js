export default ()=> {
  //safe => vue的this对象
  return {
    index: true,
    indexLabel: '序号',
    translate: false,
    searchLabelWidth: 100,
    labelWidth: 110,
    tip: false,
    align: 'center',
    headerAlign: 'center',
    border: true,
    stripe: true,
    viewBtn: true,
    selection: true,
    dialogClickModal: false,
    dialogDrag: true,
    searchMenuSpan: 6,
    searchIndex: 3,
    searchIcon: true,
    dialogWidth: 600,
    column: [{
      label: "名称",
      prop: "name",
      search: true,
      span: 24,
      rules: [{
        required: true,
        message: "请输入 名称",
        trigger: "blur"
      }]
    },{
      label: "编号",
      prop: "code",
      search: true,
      width: 200,
      span: 24,
      addDisplay: false,
      editDisplay: false
    },
      {
        label: "分类",
        row: true,
        span: 24,
        prop: "category",
        search: true,
        searchProp: 'category_eq',
        type: "radio",
        border: true,
        order: 100,
        dicUrl: window.urllist.dictUrl + 'OSS_CATEGORY',
        props: {
          label: 'name',
          value: 'code'
        },
        rules: [{
          required: true,
          message: "请选择 分类",
          trigger: "blur"
        }],
        control: (val)=> {
          if (val === 'ali') {
            return {
              internalAddress: {
                display: true,
                rules: [{
                  required: true,
                  message: "请输入 资源地址",
                  trigger: "blur"
                }]
              },
              externalAddress: {
                display: true,
                rules: []
              }
            }
          } else if (val === 'qiniu'){
            return {
              internalAddress: {
                display: false,
                rules: []
              },
              externalAddress: {
                display: true,
                rules: [{
                  required: true,
                  message: "请输入 外链地址",
                  trigger: "blur"
                }]
              }
            }
          } else {
            return {
              internalAddress: {
                display: true,
                rules: []
              },
              externalAddress: {
                display: true,
                rules: []
              }
            }
          }
        }
      },

      {
        label: "accessKey",
        row: true,
        span: 24,
        prop: "accessKey",
        search: false,
        overHidden: true,
        rules: [{
          required: true,
          message: "请输入 accessKey",
          trigger: "blur"
        }]
      },

      {
        label: "secretKey",
        row: true,
        span: 24,
        prop: "secretKey",
        search: false,
        overHidden: true,
        rules: [{
          required: true,
          message: "请输入 secretKey",
          trigger: "blur"
        }]
      },

      {
        label: "资源地址",
        row: true,
        span: 24,
        prop: "internalAddress",
        overHidden: true,
        hide: true,
        rules: []
      },
      {
        label: "外链地址",
        row: true,
        span: 24,
        prop: "externalAddress",
        overHidden: true,
        hide: true,
        rules: []
      },
      {
        label: "存储桶",
        span: 24,
        row: true,
        prop: "bucketName",
        rules: [{
          required: true,
          message: "请输入 存储桶",
          trigger: "blur"
        }]
      }],

}
}