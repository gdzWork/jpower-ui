export default ()=> {
    //safe => vue的this对象
    return {
        index: true,
        indexLabel: '序号',
        menuWidth: 300,
        translate: false,
        searchLabelWidth: 100,
        labelWidth: 110,
        tip: false,
        align: 'center',
        headerAlign: 'center',
        border: true,
        stripe: true,
        viewBtn: true,
        dateBtn: true,
        addBtn: false,
        selection: true,
        dialogClickModal: false,
        dialogDrag: true,
        searchMenuSpan: 6,
        searchIndex: 3,
        searchIcon: true,
        column: [{
            label: "文件名称",
            prop: "name",
            search: true,
            slot: true,
            rules: [{
                required: true,
                message: "请输入文件名称",
                trigger: "blur"
            }]
        },

            {
                label: "文件大小",
                prop: "fileSize",
                editDetail: true,
                slot: true
            },
            {
                label: "文件扩展名",
                prop: "fileType",
                search: true,
                searchslot: true,
                rules: [{
                    required: true,
                    message: "请输入文件扩展名",
                    trigger: "blur"
                }]
            },
            {
                label: "存储位置",
                prop: "storageType",
                bind: 'params.storageType',
                search: true,
                slot: true,
                type: 'select',
                dataType: 'string',
                dicUrl: window.urllist.dictUrl + 'FILE_STORAGE_TYPE',
                disabled: true,
                props: {
                    label: "name",
                    value: "code"
                }
            },
            {
                label: "上传时间",
                prop: "createTime",
                editDetail: true,
                slot: true,
                span: 24
            },
            {
                label: "文件标识",
                prop: "mark",
                showColumn: false,
                hide: true,
                editDetail: true,
                span: 24
            },
            {
                label: "备注",
                type: "textarea",
                prop: "note",
                showColumn: false,
                hide: true,
                span: 24
            }],
    }
}