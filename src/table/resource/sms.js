export default ()=> {
  //safe => vue的this对象
  return {
    index: true,
    indexLabel: '序号',
    translate: false,
    searchLabelWidth: 100,
    labelWidth: 110,
    menuWidth: 300,
    tip: false,
    align: 'center',
    headerAlign: 'center',
    border: true,
    stripe: true,
    viewBtn: true,
    selection: true,
    dialogClickModal: false,
    dialogDrag: true,
    searchMenuSpan: 6,
    searchIndex: 3,
    searchIcon: true,
    dialogWidth: 600,
    column: [{
      label: "名称",
      prop: "name",
      search: true,
      span: 24,
      overHidden: true,
      row: true,
      rules: [{
        required: true,
        message: "请输入 编号",
        trigger: "blur"
      }]
    },{
      label: "编号",
      prop: "code",
      search: true,
      span: 24,
      overHidden: true,
      row: true,
      rules: [{
        required: true,
        message: "请输入 编号",
        trigger: "blur"
      },
        {
          pattern: /^[a-zA-Z0-9]{1,}$/,
          message: '只能输入数字或字母'
        }]
    },
      {
        label: "分类",
        row: true,
        span: 24,
        prop: "category",
        value: 'ali',
        search: true,
        type: "radio",
        border: true,
        order: 100,
        dicUrl: window.urllist.dictUrl + 'SMS_CATEGORY',
        props: {
          label: 'name',
          value: 'code'
        },
        rules: [{
          required: true,
          message: "请选择 分类",
          trigger: "blur"
        }]
      },

      {
        label: "accessKey",
        row: true,
        span: 24,
        prop: "accessKey",
        search: false,
        overHidden: true,
        rules: [{
          required: true,
          message: "请输入 accessKey",
          trigger: "blur"
        }]
      },

      {
        label: "secretKey",
        row: true,
        span: 24,
        prop: "secretKey",
        hide: true,
        search: false,
        overHidden: true,
        rules: [{
          required: true,
          message: "请输入 secretKey",
          trigger: "blur"
        }]
      },

      {
        label: "模板ID",
        row: true,
        span: 24,
        prop: "template",
        overHidden: true,
        rules: [{
          required: true,
          message: "请输入 模板ID",
          trigger: "blur"
        }]
      },
      {
        label: "短信签名",
        row: true,
        span: 24,
        prop: "sign",
        overHidden: true,
        rules: [{
          required: true,
          message: "请输入 短信签名",
          trigger: "blur"
        }]
      },
      {
        label: "区域ID",
        span: 24,
        row: true,
        prop: "regionId",
        showColumn: false,
        hide: true
      }, {
        label: "模板参数",
        span: 24,
        prop: "parameters",
        type: 'array',
        showColumn: false,
        hide: true
      }],

}
}