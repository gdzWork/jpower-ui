// 配置编译环境和线上环境之间的切换

let baseUrl = '/api';
let iconfontVersion = ['567566_pwc3oottzol', '1066523_6bvkeuqao36'];
let iconfontUrl = `//at.alicdn.com/t/font_$key.css`;
const env = process.env
if (env.NODE_ENV == 'production') {
  baseUrl = ``; //生产环境地址
} else if (env.NODE_ENV == 'dev') {
  baseUrl = ``; // 开发环境地址
} else if (env.NODE_ENV == 'test') {
  baseUrl = `http://api.qidiangk.com`; //测试环境地址
}

Object.keys(window.urllist).forEach(key => {
  window.urllist[key] = baseUrl+window.urllist[key];
});


export const urllist = window.urllist

export {
  baseUrl,
  iconfontUrl,
  iconfontVersion,
  env
}