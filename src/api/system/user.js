
import request from '@/router/axios';
import { urllist } from '@/config/env';
import {download} from "@/util/util";
const url = urllist.user + '/core';
const system = urllist.system + '/core';
export const getRoleList = (data) => {
  return request({
    url: system + '/role/tree',
    method: 'get',
    params: data
  })
}
export const getOrgTree = (data) => {
  return request({
    url: system + '/org/tree',
    method: 'get',
    params: data
  })
}
export const getPost = (data) => {
  return request({
    url: url + '/post/select',
    method: 'get',
    params: data
  })
}
export const list = (data) => {
  return request({
    url: url + '/user/list',
    method: 'get',
    params: data
  })
}
export const addRoles = (data) => {
  return request({
    url: url + '/user/addRole',
    method: 'post',
    meta: {
      isSerialize: true
    },
    data: data
  })
}
export const resetPassword = (ids) => {
  return request({
    url: url + '/user/resetPassword',
    method: 'put',
    meta: {
      isSerialize: true
    },
    data: { ids }
  })
}
export const del = (id) => request.delete(url + '/user/delete', {
  params: {
    ids: id
  }
})
export const add = (data) => request({
  url: url + '/user/add',
  method: 'post',
  meta: {
    isSerialize: true
  },
  data: data
})
export const update = (id, data) => request({
  url: url + '/user/update',
  method: 'put',
  meta: {
    isSerialize: true
  },
  data: data
})

export const online = (userId) => {
  return request({
    url: url + '/user/online',
    method: 'get',
    params: {userId: userId}
  })
}

export const offline = (data) => request({
  url: url + '/user/offline',
  method: 'post',
  meta: {
    isSerialize: true
  },
  data: data
})

export const exportUser = async (data,options) => {
  await request.get(url + '/user/exportUser?'+data, {responseType: 'blob'}).then(resp => {
    if (resp.status === 200){
      download(resp, options);
    }else {
      this.$message.error(resp.data.message);
    }
  })
}

export const downloadTemplate = async (options) => {
  await request.get(url + '/user/downloadTemplate', {responseType: 'blob'}).then(resp => {
    if (resp.status === 200){
      download(resp, options);
    }else {
      this.$message.error(resp.data.message);
    }
  })
}
