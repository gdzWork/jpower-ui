
import request from '@/router/axios';
import { urllist } from '@/config/env';
const url = urllist.system+"/core/menu";

export const update = (id, data) => request({
  url: url + '/update',
  method: 'put',
  meta: {
    isSerialize: true
  },
  data: data
})
export const statusSwitch = (id, status) => request({
  url: url + '/switch',
  method: 'put',
  meta: {
    isSerialize: true
  },
  data: {
    id: id,
    status: status
  }
})
export const add = (data) => request({
  url: url + '/add',
  method: 'post',
  meta: {
    isSerialize: true
  },
  data: data
})
export const del = (id) => request.delete(url + '/delete', {
  params: {
    ids: id
  }
})
export const list = (data) => {

  if (data.hasOwnProperty('status')){
    data['status_eq'] = data.status;
    delete data['status'];
  }

  return request({
    url: url + '/list',
    method: 'get',
    meta: {
      isSerialize: true
    },
    params: data
  })
}

export const clientList = () => {
  return request.get(urllist.system + '/core/client/selectList')
};

export const listFunction = (id) => {
  return request({
    url: url + '/listFunction',
    method: 'get',
    meta: {
      isSerialize: true
    },
    params: {
      clientId: id
    }
  })
};

export const listFunctionId = (id) => {
  return request({
    url: url + '/listFunctionId',
    method: 'get',
    meta: {
      isSerialize: true
    },
    params: {
      menuId: id
    }
  })
};

export const saveFunction = (data) => request({
  url: url + '/saveFunction',
  method: 'post',
  meta: {
    isSerialize: true
  },
  data: data
});