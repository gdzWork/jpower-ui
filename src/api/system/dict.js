
import request from '@/router/axios';
import { urllist } from '@/config/env';
import {validatenull} from "@/util/validate";
const url = urllist.system + '/core';

export const listDictChildList = (id) => request.get(url + '/dict/listDictChildList', {
  params: {
    parentId: id
  }
})
export const del = (id) => request.delete(url + '/dict/deleteDict', {
  params: {
    ids: id
  }
})
export const addType = (data) => request({
  url: url + '/dict/add',
  method: 'post',
  meta: {
    isSerialize: true
  },
  data: data
});
export const update = (id, data) => {

  if(data.hasOwnProperty("parentId") && validatenull(data.parentId)){
    data.parentId = "-1";
  }

  return request({
    url: url + '/dict/saveDict',
    method: 'post',
    meta: {
      isSerialize: true
    },
    data: data
  });
}
export const add = (data) => request({
  url: url + '/dict/saveDict',
  method: 'post',
  meta: {
    isSerialize: true
  },
  data: data
})
export const list = (data) => request.get(url + '/dict/listByType', {
  params: data
})
export const getDictType = (id) => request.get(url + '/dict/getDictType', {
  params: {
    id
  }
});
export const delType = (id) => request.delete(url + '/dict/deleteDictType', {
  params: {
    ids: id
  }
})
export const updateType = (data) => {

  delete data.createOrg;
  delete data.createUser;
  delete data.createTime;
  delete data.updateUser;
  delete data.updateTime;
  delete data.status;
  delete data.params;

  if(validatenull(data.parentId)){
    data.parentId = "-1";
  }

  return request({
    url: url + '/dict/update',
    method: 'post',
    meta: {
      isSerialize: true
    },
    data: data
  });
}
export const dictTypeTree = (data) => request.get(url + '/dict/dictTypeTree', {
  params: data
});

export const stop = (ids) => request({
  url: url + '/dict/stopDict',
  method: 'post',
  meta: {
    isSerialize: true
  },
  data: {
    ids: ids
  }
})