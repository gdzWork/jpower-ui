
import request from '@/router/axios';
import { urllist } from '@/config/env';
import {validatenull} from "@/util/validate";
const url = urllist.system

export const add = (data) => request({
  url: url + '/core/function/add',
  method: 'post',
  meta: {
    isSerialize: true
  },
  data: data
})
export const update = (id, data) => {

  if(data.hasOwnProperty("parentId") && validatenull(data.parentId)){
    data.parentId = "-1";
  }

  return request({
    url: url + '/core/function/update',
    method: 'put',
    meta: {
      isSerialize: true
    },
    data: data
  })
}
export const del = (id) => request.delete(url + '/core/function/delete', {
  params: {
    ids: id
  }
})
export const menuTree = (data = {}) => {

  if (data.topMenuId === ''){
    delete data.topMenuId;
  }

  return request.get(url + '/core/function/menuTree', {
    params: data
  });
};

export const list = (data = {}) => {

  if (data.hasOwnProperty('functionType')){
    data['functionType_eq'] = data.functionType;
    delete data['functionType'];
  }

  return request.get(url + '/core/function/listByParent', {
    params: data
  })
};

export const clientList = () => {
  return request.get(url + '/core/client/selectList')
};

export const menuList = (clientId) => {
  return request.get(url + '/core/menu/select', {
    params: {clientId:clientId}
  })
};

export const generate = () => {
  return request({
    url: url + '/core/function/generate',
    method: 'post'
  })
};

export const hide = (id,hide) => {
  return request({
    url: url + '/core/function/hide',
    method: 'post',
    meta: {
      isSerialize: true
    },
    data: {
      id:id,
      hide:hide
    }
  })
};

export const saveHierarchy = (ids,parentId) => {
  return request({
    url: url + '/core/function/saveHierarchy',
    method: 'post',
    meta: {
      isSerialize: true
    },
    data: {
      ids: ids,
      parentId: parentId
    }
  })
};


export const treeHierarchy = (data = {}) => {
  return request.get(url + '/core/function/treeMenuTypeByClientId', {
    params: data
  });
};