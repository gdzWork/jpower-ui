
import request from '@/router/axios';
import { urllist } from '@/config/env';
const url = urllist.user+"/core/post";

export const update = (id, data) => request({
  url: url + '/update',
  method: 'put',
  meta: {
    isSerialize: true
  },
  data: data
})
export const add = (data) => request({
  url: url + '/add',
  method: 'post',
  meta: {
    isSerialize: true
  },
  data: data
})
export const del = (id) => request.delete(url + '/delete', {
  params: {
    ids: id
  }
})
export const list = (data) => {

  if (data.hasOwnProperty('type')){
    data['type_eq'] = data.type;
    delete data['type'];
  }

  if (data.hasOwnProperty('status')){
    data['status_eq'] = data.status;
    delete data['status'];
  }

  return request({
    url: url + '/list',
    method: 'get',
    meta: {
      isSerialize: true
    },
    params: data
  })
}