
import request from '@/router/axios';
import { urllist } from '@/config/env';
const url = urllist.system + '/core';

export const list = (data = {}) => request.get(url + '/dataScope/listDataByParent', {
  params: Object.assign({functionType_eq: 1}, data)
})
export const updateDataScope = (row) => {

  delete row.createOrg;
  delete row.createUser;
  delete row.createTime;
  delete row.updateTime;
  delete row.updateUser;
  delete row.params;

  return request({
    url: url + '/dataScope/update',
    method: 'put',
    meta: {
      isSerialize: true
    },
    data: row
  })
}
export const removeDataScope = (id) => {
  return request({
    url: url + '/dataScope/delete',
    method: 'delete',
    params: {
      id
    }
  })
}

export const copy = (id) => {
  debugger
  return request({
    url: url + '/dataScope/copy',
    method: 'post',
    meta: {
      isSerialize: true
    },
    data: {
      id
    }
  })
}

export const addDataScope = (row) => {
  delete row.id;
  delete row.createOrg;
  delete row.createUser;
  delete row.createTime;
  delete row.updateUser;
  delete row.updateTime;

  return request({
    url: url + '/dataScope/add',
    method: 'post',
    meta: {
      isSerialize: true
    },
    data: row
  })
}
export const getListDataScope = (params) => {
  return request({
    url: url + '/dataScope/listPage',
    method: 'get',
    params: params
  })
}

export const clientList = () => {
  return request.get(url + '/client/selectList')
};

export const menuList = (clientId) => {
  return request.get(url + '/menu/select', {
    params: {clientId:clientId}
  })
};