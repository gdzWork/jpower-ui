
import request from '@/router/axios';
import { urllist } from '@/config/env';
const url = urllist.file

export const detail = (id) => request.get(url + '/resource/file/get', {
    params: {
        id: id
    }
})
export const del = (id) => request.delete(url + '/resource/file/delete', {
    params: {
        ids: id
    }
})
export const update = (id, data) => request({
    url: url + '/resource/file/update',
    method: 'put',
    meta: {
        isSerialize: true
    },
    data: data
})
export const list = (data) => request.get(url + '/resource/file/listPage', {
    params: data
})
export const storageType = () => request.get(url + '/resource/file/storageType')

export const getUrl = (mark) => request.get(url + '/resource/file/url/'+mark)
