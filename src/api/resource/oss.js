
import request from '@/router/axios';
import { urllist } from '@/config/env';
const url = urllist.file

export const add = (data) => {
  return request({
    url: url + '/oss/add',
    method: 'post',
    data: data
  });
}

export const update = (id, data) => {
  return request({
    url: url + '/oss/update',
    method: 'put',
    data: data
  });
}
export const del = (ids) => request.delete(url + '/oss/delete/'+ids)

export const list = (data) => {
  return request.get(url + '/oss/list', {
    params: data
  });
}
