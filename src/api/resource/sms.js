
import request from '@/router/axios';
import { urllist } from '@/config/env';
import func from "@/util/func";
const url = urllist.file

export const add = (data) => {

  data.parameters = func.join(data.parameters);

  return request({
    url: url + '/sms/add',
    method: 'post',
    data: data
  });
}

export const update = (id, data) => {


  if (Array.isArray(data.parameters)){
    data.parameters = func.join(data.parameters);
  }

  return request({
    url: url + '/sms/update',
    method: 'put',
    data: data
  });
}
export const del = (id) => request.delete(url + '/sms/delete', {
  params: {
    ids: id
  }
})
export const list = (data) => {

  data.category_eq = data.category;
  delete data.category;

  return request.get(url + '/sms/list', {
    params: data
  });
}

export const test = (data) => request({
  url: url + '/sms/test',
  method: 'post',
  data: data
});