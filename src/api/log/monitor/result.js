
import request from '@/router/axios';
import { urllist } from '@/config/env';
import {download} from "@/util/util";
const url = urllist.log


export const list = (data) => request.get(url + '/monitor/log/list', {
  params: data
})


export const excel = (data) => request.get(url + '/monitor/log/export', {
  params: data
})

export const server = (data) => request.get(url + '/monitor/setting/servers', {
  params: data
})

export const exportResult = async (data,options) => {
  await request.get(url + '/monitor/log/export?'+data, {responseType: 'blob'}).then(resp => {
    if (resp.status === 200){
      download(resp, options);
    }else {
      this.$message.error(resp.data.message);
    }
  })
}