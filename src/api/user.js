import request from '@/router/axios';
import { urllist } from '@/config/env';
import { getAuthorization } from '@/util/util';
import website from '@/config/website';
import store from '@/store/';
export const loginByUsername = (data,header) => {
  return request({
    url: urllist.auth + '/auth/login',
    method: 'post',
    headers: Object.assign(header, {
      'Authorization': getAuthorization(website.clientCode, website.clientSecret),
      'User-Type': 'web',
    }),
    meta: {
      isToken: false,
      isSerialize: true,
    },
    data: data
  })
}

export const getTenantCode = () => {
  return request({
    url: urllist.system + '/core/tenant/queryByDomain',
    method: 'get',
    headers: {
      'Authorization': getAuthorization(website.clientCode, website.clientSecret),
      'User-Type': 'web',
    },
    meta: {
      isToken: false,
    },
    params: {
      domain: window.location.href.substring(0, window.location.href.indexOf('#') - 1)
    }
  })
}

export const refreshToken = (refreshToken) => {
  return request({
    url: urllist.auth + '/auth/login',
    method: 'post',
    headers: {
      'Authorization': getAuthorization(website.clientCode, website.clientSecret),
      'User-Type': 'web',
    },
    meta: {
      isToken: false,
      isSerialize: true
    },
    data: {
      refreshToken: refreshToken,
      grantType: 'refresh_token',
      tenantCode: store.getters.tenantCode
    }
  })
}

export const sendCode = (phone) => {
  return request({
    url: urllist.auth + '/auth/captcha/'+phone,
    method: 'post'
  })
}
export const resetPassword = (data) => {
  return request({
    url: urllist.user + '/core/user/updatePassword',
    method: 'get',
    meta: {
      isSerialize: true
    },
    params: data
  })
}

export const getMenu = (id) => request({
  url: urllist.system + '/core/function/listMenuTree',
  method: 'get',
  params: {
    topMenuId: id
  }
})

export const getBtnCode = () => request({
  url: urllist.system + '/core/function/listBut',
  method: 'get'
})

export const getTopMenu = () => request({
  url: urllist.system + '/core/menu/roleMenu',
  method: 'get'
});

export const getCaptcha = () => request({
  url: urllist.auth + '/auth/captcha',
  method: 'get'
})

export const logout = (data) => request({
  url: urllist.auth + '/auth/loginOut',
  meta: {
    isSerialize: true
  },
  method: 'post',
  data: data
})

export const updatePhone = (data) => request({
  url: urllist.user + '/core/user/updatePhone',
  method: 'post',
  data: data
})

export const sendEmailCode = (email) => request({
  url: urllist.auth + '/auth/sendEmailCode/'+email,
  method: 'post'
})

export const updateEmail = (data) => request({
  url: urllist.user + '/core/user/updateEmail',
  method: 'post',
  data: data
})
