// 系统地址
window.urllist = {
  // 系统管理
  system: '/jpower-system',
  // 用户管理
  user: '/jpower-user',
  //统一授权
  auth: '/jpower-auth',
  //文件管理
  file: '/jpower-file',
  //日志管理
  log: '/jpower-log',

  //字典请求地址
  dictUrl: '/jpower-system/core/dict/getDictListByType?dictTypeCode=',
  //租户选择项列表
  tenantSelectors: '/jpower-system/core/tenant/selectors',
  //文件上传地址
  upload: '/jpower-file/resource/file/upload',
  //文件下载
  download: '/jpower-file/resource/file/download/'
}
window.$KEY = (function () {
  return {
    AUTH: 'jpower-auth'
  }
})();
